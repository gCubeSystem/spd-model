package org.gcube.data.spd.model.service.types;

public enum JobStatus {

	PENDING, 
	RUNNING,
	FAILED,
	COMPLETED
}
